#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct PriorityQueue* PriorityQueue;

PriorityQueue PriorityQueue_create();
void PriorityQueue_free(PriorityQueue pq);
int PriorityQueue_insert(PriorityQueue pq, void *data, uint64_t priority);
int PriorityQueue_pop(PriorityQueue pq, void **data, uint64_t *priority);
bool PriorityQueue_is_empty(PriorityQueue pq);
void PriorityQueue_print_heap(PriorityQueue pq, FILE *destination);

#endif // PRIORITY_QUEUE_H
