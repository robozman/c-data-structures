#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define HEAP_BASE_SIZE 100

#include "PriorityQueue.h"

struct PriorityQueueElement {
        void *data;
        uint64_t priority;
};

struct PriorityQueue {
        struct PriorityQueueElement *heap;
        size_t heap_size;
        size_t heap_usage;
};


PriorityQueue PriorityQueue_create()
{
        PriorityQueue pq = malloc(sizeof(struct PriorityQueue));
        if (pq == NULL) {
                return NULL;
        }
        pq->heap = calloc(HEAP_BASE_SIZE, sizeof(struct PriorityQueueElement));
        if (pq->heap == NULL) {
                free(pq);
                return NULL;
        }
        pq->heap_size = HEAP_BASE_SIZE;
        pq->heap_usage = 0;
        return pq;
}

void PriorityQueue_free(PriorityQueue pq)
{
        free(pq->heap);
        free(pq);
}


static
int PriorityQueue_increase_size(PriorityQueue pq)
{
        pq->heap = reallocarray(pq->heap, pq->heap_size * 2, sizeof(struct PriorityQueueElement));
        if (pq->heap == NULL) {
                free(pq);
                return -1;
        }

        memset(pq->heap + pq->heap_size, 0, pq->heap_size * sizeof(struct PriorityQueueElement));
        
        pq->heap_size = pq->heap_size * 2;
        return 0;
}

static 
void PriorityQueue_sift_up(PriorityQueue pq)
{
        bool is_root = (pq->heap_usage == 0);
        size_t index = pq->heap_usage;
        while (!is_root) {
                uint64_t current_priority = pq->heap[index].priority;
                uint64_t parent_priority = pq->heap[(index-1)/2].priority;
                if (parent_priority < current_priority) {
                        struct PriorityQueueElement temp = pq->heap[index];
                        pq->heap[index] = pq->heap[(index-1)/2];
                        pq->heap[(index-1)/2] = temp;
                }
                index = (index-1)/2;
                is_root = (index == 0);
        }
}

static
void PriorityQueue_sift_down(PriorityQueue pq)
{
        struct PriorityQueueElement child1 = pq->heap[1];
        struct PriorityQueueElement child2 = pq->heap[2];
        bool is_leaf = ((child1.priority == 0) && (child2.priority == 0));
        size_t current_index = 0;

        while(!is_leaf) {
                if ((child1.priority < pq->heap[current_index].priority) &&
                    (child2.priority < pq->heap[current_index].priority)) {
                        break;
                }
                else if (child1.priority > child2.priority) {
                        pq->heap[(2*current_index) + 1] = pq->heap[current_index];
                        pq->heap[current_index] = child1;
                        current_index = (2*current_index)+1;
                }
                else {
                        pq->heap[(2*current_index) + 2] = pq->heap[current_index];
                        pq->heap[current_index] = child2;
                        current_index = (2*current_index)+2;
                }


                child1 = pq->heap[(2 * current_index) + 1];
                child2 = pq->heap[(2 * current_index) + 2];
                is_leaf = ((child1.priority == 0) && (child2.priority == 0));
        }
}



int PriorityQueue_insert(PriorityQueue pq, void *data, uint64_t priority)
{
        if (priority == 0) {
                return -1;
        }

        if (pq->heap_usage == pq->heap_size) {
                int ret = PriorityQueue_increase_size(pq);
                if (ret < 0) {
                        return -1;
                }
        }
        
        pq->heap[pq->heap_usage].data = data;
        pq->heap[pq->heap_usage].priority = priority;
        
        PriorityQueue_sift_up(pq);
        pq->heap_usage++;
        return 0;
}


int PriorityQueue_pop(PriorityQueue pq, void **data, uint64_t *priority)
{
        if (pq->heap_usage == 0) {
                return -1;
        }

        if (data != NULL) {
                *data = pq->heap[0].data;
        }
        if (priority != NULL) {
                *priority = pq->heap[0].priority;
        }

        pq->heap[0].data = pq->heap[pq->heap_usage - 1].data; 
        pq->heap[0].priority = pq->heap[pq->heap_usage - 1].priority;
        pq->heap[pq->heap_usage - 1].data = NULL;
        pq->heap[pq->heap_usage - 1].priority = 0;

        PriorityQueue_sift_down(pq);
        pq->heap_usage--;
        return 0; 
}

bool PriorityQueue_is_empty(PriorityQueue pq)
{
        return (pq->heap_usage == 0);
}

void PriorityQueue_print_heap(PriorityQueue pq, FILE *destination)
{
        printf("[ ");
        for (size_t i = 0; i < pq->heap_usage; i++) {
                char *print_string = i == pq->heap_usage - 1 ? "%lu " : "%lu, ";
                fprintf(destination, print_string, pq->heap[i].priority);
        }
        printf("]\n");
}
