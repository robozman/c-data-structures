#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "PriorityQueue.h"

int main(int argc, char *argv[])
{

        int ret;

        PriorityQueue pq = PriorityQueue_create();
        if (pq == NULL) {
                fprintf(stderr, "Could not allocate priority queue\n");
                return 0;
        }

        for (uint64_t i = 1; i < 1000; i++) {
                ret = PriorityQueue_insert(pq, (void*)i, i);
                if (ret < 0) {
                        fprintf(stderr, "Could not insert into priority queue\n");
                        return -1;
                }
        }

        
        
        void *data;
        uint64_t priority;

        while(!PriorityQueue_is_empty(pq)) {
                PriorityQueue_print_heap(pq, stdout);
                ret = PriorityQueue_pop(pq, &data, &priority);
                if (ret < 0) {
                        fprintf(stderr, "Could not pop from priority queue\n");
                }
                printf("Data: %p, Priority: %lu\n", data, priority);
        }
       
        PriorityQueue_free(pq);
        
        
        return 0;
}
